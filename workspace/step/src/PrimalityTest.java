import static org.junit.Assert.*;

import org.junit.Test;


public class PrimalityTest {

	@Test
	public void test2() {
	
		assertTrue(Primality.isPrime(2));
	}
	@Test
	public void test4() {
		assertFalse(Primality.isPrime(4));

	}

	@Test
	public void test7() {
		assertFalse(Primality.isPrime(7));

	}

}
